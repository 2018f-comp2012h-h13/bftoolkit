#include "BFProgram.h"
#include <deque>

namespace BFToolKit{

// BFProgram class constructor
BFProgram::BFProgram(const std::string &code): Program(), Transpiler(), code {} {
	std::deque<char> deque;
	char opposite;
	for (const char &c : code)
		switch (c) {
		case '>':
		case '<':
		case '+':
		case '-':
		case ']':
			opposite = c == '>' ?
					'<' :
				c == '<' ?
					'>' :
				c == '+' ?
					'-' :
				c == '-' ?
					'+' :
					'[';
			if (deque.empty() || deque.back() != opposite)
				deque.push_back(c);
			else
				deque.pop_back();
			break;
		case '[':
		case '.':
		case ',':
			deque.push_back(c);
			break;
		}
	while (!deque.empty()) {
		this->code += deque.front();
		deque.pop_front();
	}
}

// Program execution (non-interactive)
std::string BFProgram::execute(const std::string &input) const {

	std::string output ="";
	char cell[30000];
	for (int i=0; i<30000;i++)
		cell[i]=0;
	int inPos =0;
	int dPos=0;
	int pair =0;
	int ip=0;
	do{
		char command = code[inPos];
		switch(command){
			case '+':
				cell[dPos]++;
				break;
			case '-':
				cell[dPos]--;
				break;
			case '>':
				++dPos;
				break;
			case '<':
				--dPos;
				break;

			case '[':

			if(cell[dPos]==0){
				for(int i =inPos+1; i<code.length();i++){
					if(code[i]=='[')
					pair+=1;
					else if (code[i]==']')
						if (pair>0)
								pair-=1;
						else {inPos =i;break;}
				}
			}
				break;
			case ']':

			if(cell[dPos]!=0){
				for(int i =inPos-1; i>0; i--){
					if(code[i]==']')
					pair+=1;
					else if (code[i]=='[')
						if (pair>0)
								pair-=1;
						else {inPos =i;break;}
				}
			}
				break;
			case ',':
				cell[dPos]= input[ip];
				ip+=1;
				break;
			case '.':
				output +=cell[dPos];// done
				break;
			default:break;
		}
	inPos++;
	} while(inPos!=code.length());
	return output;
}

// BF -> C99 conversion
std::string BFProgram::transpile_to_c() const {
	std::string result = "#include <stdio.h>\n"
		"\n"
		"int main(int argc, char *argv[]) {\n"
		"  unsigned char tape[30000];\n"
		"  for (int i = 0; i < 30000; ++i)\n"
		"    tape[i] = 0;\n"
		"  unsigned char *ptr = tape;\n";
	int indent_level = 1, count;
	char cmd;
	for (int i = 0; i < code.size(); ++i)
		switch (code[i]) {
		case '>':
		case '<':
		case '+':
		case '-':
			count = 1;
			cmd = code[i++];
			while (i < code.size() && code[i] == cmd) {
				++count;
				++i;
			}
			--i;
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += cmd == '>' ?
					"ptr += " :
				cmd == '<' ?
					"ptr -= " :
				cmd == '+' ?
					"*ptr += " :
					"*ptr -= ";
			result += std::to_string(count);
			result += ";\n";
			break;
		case '.':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "putchar(*ptr);\n";
			break;
		case ',':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "*ptr = *argv[1]++;\n";
			break;
		case '[':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "while (*ptr) {\n";
			++indent_level;
			break;
		case ']':
			--indent_level;
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "}\n";
			break;
		}
	result += "  return 0;\n"
		"}\n";
	return result;
}

// BF -> Java 8 conversion
std::string BFProgram::transpile_to_java() const {
	std::string result = "public class Program {\n"
		"  \n"
		"  public static void main(String[] args) {\n"
		"    byte[] tape = new byte[30000];\n"
		"    int ptr = 0, inputIndex = 0;\n";
	int indent_level = 2, count;
	char cmd;
	for (int i = 0; i < code.size(); ++i)
		switch (code[i]) {
		case '>':
		case '<':
		case '+':
		case '-':
			count = 1;
			cmd = code[i++];
			while (i < code.size() && code[i] == cmd) {
				++count;
				++i;
			}
			--i;
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += cmd == '>' ?
					"ptr += " :
				cmd == '<' ?
					"ptr -= " :
				cmd == '+' ?
					"tape[ptr] += " :
					"tape[ptr] -= ";
			result += std::to_string(count);
			result += ";\n";
			break;
		case '.':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "System.out.print((char)tape[ptr]);\n";
			break;
		case ',':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "tape[ptr] = inputIndex < args[0].length() ? (byte)args[0].charAt(inputIndex++) : 0;\n";
			break;
		case '[':
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "while (tape[ptr] != 0) {\n";
			++indent_level;
			break;
		case ']':
			--indent_level;
			for (int i = 0; i < indent_level; ++i)
				result += "  ";
			result += "}\n";
			break;
		}
	result += "  }\n"
		"}\n";
	return result;
}

// BF -> Python 3 conversion
std::string BFProgram::transpile_to_python() const {
	std::string result = "import sys\n"
					"data = [0 for x in range(30000)]\n"
					"inputIndex = 0\n"
					"index = 0\n\n";
	int indent_level = 0, count;
	char cmd;
	for (unsigned int i = 0; i < code.size(); ++i){
		switch (code[i]) {
		case '>':
		case '<':
		case '+':
		case '-':
			count = 1;
			cmd = code[i++];
			while (i < code.size() && code[i] == cmd) {
				++count;
				++i;
			}
			--i;
			for (int i = 0; i < indent_level; ++i)
				result += "\t";
			result += cmd == '>' ?
					"index += " :
				cmd == '<' ?
					"index -= " :
				cmd == '+' ?
					"data[index] += " :
					"data[index] -= ";
			result += std::to_string(count);
			if (cmd=='+' || cmd=='-'){
				result += "\n";
				for (int i=0; i<indent_level; ++i)
					result += "\t";
				result += "data[index] = (data[index]%256+256)%256";
			}
			result += "\n";
			break;
		case '.':
			for (int i = 0; i < indent_level; ++i)
				result += "\t";
			result += "print(chr(data[index]), end = \"\")\n";
			break;
		case ',':
			for (int i = 0; i < indent_level; ++i)
				result += "\t";
			result += "data[index] = ord(sys.argv[1][inputIndex]) if inputIndex < len(sys.argv[1]) else 0\n";
			for (int i = 0; i < indent_level; ++i)
							result += "\t";
			result += "inputIndex += 1\n";
			break;
		case '[':
			result += "\n";
			for (int i = 0; i < indent_level; ++i)
				result += "\t";
			result += "while data[index] != 0:\n";
			++indent_level;
			break;
		case ']':
			--indent_level;
			break;
		}
	}
	return result;
}

}
