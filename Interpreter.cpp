#include "Interpreter.h"

namespace BFToolKit {
// operator() is simply an alias for interpret() - see
// Interpreter.h for more details
std::unique_ptr<Program> Interpreter::operator()(const std::string &code) const {
	return interpret(code);
}
}
