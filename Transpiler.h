#ifndef TRANSPILER_H_
#define TRANSPILER_H_

#include <string>

namespace BFToolKit {
class Transpiler {
	/*
	 * Abstract class for modeling a generic transpiler, i.e. a compiler between
	 * different high-level programming languages
	 * Any instance of Transpiler should support at least the following conversions:
	 * - Conversion to C source code
	 * - Conversion to Java source code
	 * - Conversion to Python source code
	 * Note that a Transpiler instance need not provide functionality for directly
	 * writing the transpilation output to (a) given file(s) - it is up to the user
	 * of the Transpiler instance and/or the deriving class(es) to do so
	 */
public:
	// Transpilation to C program code
	virtual std::string transpile_to_c() const = 0;
	// Transpilation to Java program code
	virtual std::string transpile_to_java() const = 0;
	// Transpilation to Python program code
	virtual std::string transpile_to_python() const = 0;

	virtual ~Transpiler() = default;
};
}

#endif
