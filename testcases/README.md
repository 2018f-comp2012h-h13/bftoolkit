### Disclaimer

The programs contained within this directory may be taken from other sources and are therefore not under the MIT License for BFToolKit.  Instead, if individual program files contain a copyright/license notice, please refer to those instead.