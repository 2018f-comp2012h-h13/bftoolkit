#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include <string>
#include <memory>
#include "Program.h"

namespace BFToolKit {
class Program;
class Interpreter {
	/*
	 * Abstract base class for modeling an interpreter for an
	 * arbitrary programming language
	 */

public:
	// An interpreter should be able to interpret a program
	// in a given programming language and produce a corresponding
	// Program object
	virtual std::unique_ptr<Program> interpret(const std::string &code) const = 0;

	// Alias for Interpreter::interpret(std::string) - each interpreter
	// should be a functor, i.e. can be invoked like an ordinary function
	virtual std::unique_ptr<Program> operator()(const std::string &code) const;

	virtual ~Interpreter() = default;
};
}

#endif
