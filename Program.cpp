#include "Program.h"
#include <iostream>

namespace BFToolKit {

// operator() is just an alias for execute() - see Program.h
// for more details
std::string Program::operator()(const std::string &input) const {
	return execute(input);
}

// Execute given program in interactive mode
void Program::interactive() const {
	std::cout << "Please enter the program input: ";
	char input[1025]; // Accept 1KB of program input at most, excluding the NUL terminator
	std::cin.getline(input, 1025);
	std::cout << "The program output is: " << execute(input) << std::endl;
}

}
