#ifndef TRIVIAL_BF_SUBSTITUTION_H_
#define TRIVIAL_BF_SUBSTITUTION_H_

#include "Interpreter.h"

namespace BFToolKit {
class TrivialBFSubstitution : public Interpreter {
	/*
	 * A multi-purpose interpreter for interpreting arbitrary
	 * TrivialBrainf**kSubstitutions based on substitution tokens
	 * determined during object construction
	 * Each program interpreted is converted into a corresponding
	 * BFProgram object regardless of the original TrivialBrainf**kSubstituion
	 * used
	 */

public:

	// Class constructor
	// Token substitutions are performed in the following order:
	// 1) ">" -> RIGHT_ANGLED_BRACKET
	// 2) "<" -> LEFT_ANGLED_BRACKET
	// 3) "+" -> PLUS_SIGN
	// 4) "-" -> MINUS_SIGN
	// 5) "." -> PERIOD
	// 6) "," -> COMMA
	// 7) "[" -> LEFT_SQUARE_BRACKET
	// 8) "]" -> RIGHT_SQUARE_BRACKET
	// where each token on the left (e.g. "-") take their usual meaning
	// in ordinary Brainf**k
	// E.g. A ReverseF**k interpreter would be:
	// TrivialBFSubstitution("<", ">", "-", "+", ",", ".", "]", "[")
	TrivialBFSubstitution(
		const std::string &right_angled_bracket,
		const std::string &left_angled_bracket,
		const std::string &plus_sign,
		const std::string &minus_sign,
		const std::string &period,
		const std::string &comma,
		const std::string &left_square_bracket,
		const std::string &right_square_bracket
	);

	// Interpret the given program under the specified TrivialBrainf**kSubstitution
	virtual std::unique_ptr<Program> interpret(const std::string &code) const override;

private:
	const std::string
		RIGHT_ANGLED_BRACKET,
		LEFT_ANGLED_BRACKET,
		PLUS_SIGN,
		MINUS_SIGN,
		PERIOD,
		COMMA,
		LEFT_SQUARE_BRACKET,
		RIGHT_SQUARE_BRACKET;
};
}

#endif
