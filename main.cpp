#include <iostream>
#include <fstream>
#include <cstring>
#include "Interpreter.h"
#include "Program.h"
#include "Transpiler.h"
#include "TrivialBFSubstitution.h"
#include "BFProgram.h"

int main() {
	std::cout << "Welcome to the BFToolKit CLI!" << std::endl <<
		"This program aims to demonstrate some possible uses of the BFToolKit library." << std::endl <<
		"Please refer to https://gitlab.com/2018f-comp2012h-h13/bftoolkit for more information on how to use this library." << std::endl << std::endl;
	while (true) {
		std::cout << "N. Create an interpreter for an arbitrary TrivialBrainf**kSubstitution" << std::endl <<
			"D. Use the default Brainf**k interpreter" << std::endl <<
			"Q. Exit the program" << std::endl << std::endl <<
			"Please select an option: ";
		char option;
		std::cin >> option;
		option = toupper(option);
		std::cout << std::endl;
		if (option == 'Q')
			break;
		switch (option) {
		case 'N':
			{
				const char BF_CMDS[9] {"><+-.,[]"};
				char substitutions[8][101];
				bool is_valid = true, has_empty_substitution = false;
				for (int i = 0; i < 8; ++i) {
					std::cout << "Please enter a substitution for the `" << BF_CMDS[i] << "` command (max 100 chars): ";
					if (i == 0)
						std::cin.ignore();
					std::cin.getline(substitutions[i], 101);
					if (*substitutions[i] == '\0') {
						has_empty_substitution = true;
						break;
					}
					for (int j = 0; j < i; ++j)
						if (strcmp(substitutions[i], substitutions[j]) == 0) {
							is_valid = false;
							break;
						}
					if (!is_valid)
						break;
				}
				std::cout << std::endl;
				if (has_empty_substitution)
					std::cerr << "You may not substitute a Brainf**k command with an empty token!" << std::endl;
				else if (!is_valid)
					std::cerr << "Duplicate substitution tokens detected - you may not substitute two or more Brainf**k commands with the same token!" << std::endl;
				else {
					BFToolKit::TrivialBFSubstitution interpreter {
						substitutions[0],
						substitutions[1],
						substitutions[2],
						substitutions[3],
						substitutions[4],
						substitutions[5],
						substitutions[6],
						substitutions[7]
					};
					while (true) {
						std::cout << "F. Read a program from a file" << std::endl <<
							"I. Read a program from standard input" << std::endl <<
							"Q. Discard the current interpreter" << std::endl << std::endl <<
							"Please select an option: ";
						char option;
						std::cin >> option;
						option = toupper(option);
						std::cout << std::endl;
						if (option == 'Q')
							break;
						switch (option) {
						case 'F':
							{
								char filename[101];
								bool file_exists, first_time = true;
								do {
									std::cout << "Please enter a filename (max 100 chars): ";
									if (first_time) {
										std::cin.ignore();
										first_time = false;
									}
									std::cin.getline(filename, 101);
									file_exists = static_cast<bool>(std::ifstream {filename});
									if (!file_exists)
										std::cerr << "The file you specified does not exist!" << std::endl;
								} while (!file_exists);
								std::ifstream program_file {filename};
								std::string code;
								char temp;
								while (program_file.get(temp))
									code += temp;
								program_file.close();
								BFToolKit::BFProgram program {*static_cast<BFToolKit::BFProgram *>(interpreter(code).get())};
								{
									char filename[101];
									bool file_exists;
									while (true) {
										std::cout << "S. Read program input from standard input" << std::endl <<
											"F. Read program input from file" << std::endl <<
											"C. Transpile program to C and write result to file" << std::endl <<
											"J. Transpile program to Java and write result to file" << std::endl <<
											"P. Transpile program to Python and write result to file" << std::endl <<
											"Q. Discard current program" << std::endl << std::endl <<
											"Please enter an option: ";
										char option;
										std::cin >> option;
										option = toupper(option);
										std::cout << std::endl;
										char input[1025], save_option;
										std::ifstream input_file;
										std::string output;
										if (option == 'Q')
											break;
										switch (option) {
										case 'S':
											std::cout << "Enter your program input (max 1KB): ";
											std::cin.ignore();
											std::cin.getline(input, 1025);
											output = program(input);
											std::cout << "The program output is: " << output << std::endl;
											do {
												std::cout << "Save program output to file (Y/N)? ";
												std::cin >> save_option;
												save_option = toupper(save_option);
												if (save_option != 'Y' && save_option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (save_option != 'Y' && save_option != 'N');
												if (save_option == 'Y') {
												char filename[101];
												bool file_exists;
												do {
													std::cout << "Enter a filename (max 100 chars): ";
													std::cin.ignore();
													std::cin.getline(filename, 101);
													file_exists = static_cast<bool>(std::ifstream {filename});
													if (file_exists) {
														std::cout << "The file with the given name already exists." << std::endl;
														char option;
														do {
															std::cout << "Do you want to overwrite it (Y/N)? ";
															std::cin >> option;
															option = toupper(option);
															if (option != 'Y' && option != 'N')
																std::cerr << "Invalid option - please select one of Y and N." << std::endl;
														} while (option != 'Y' && option != 'N');
														if (option == 'Y')
															break;
													}
												} while (file_exists);
												std::ofstream output_file {filename};
												output_file << output;
												output_file.close();
											}
											break;
										case 'F':
											{
												bool first_time = true;
												do {
													std::cout << "Enter the filename (max 100 chars): ";
													if (first_time) {
														std::cin.ignore();
														first_time = false;
													}
													std::cin.getline(filename, 101);
													file_exists = static_cast<bool>(std::ifstream {filename});
													if (!file_exists)
														std::cerr << "The file you specified does not exist." << std::endl;
												} while (!file_exists);
											}
											input_file.open(filename);
											{
												char temp;
												while (input_file.get(temp))
													output += temp;
											}
											input_file.close();
											output = program(output);
											std::cout << "The program output is: " << output << std::endl;
											do {
												std::cout << "Save program output to file (Y/N)? ";
												std::cin >> save_option;
												save_option = toupper(save_option);
												if (save_option != 'Y' && save_option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (save_option != 'Y' && save_option != 'N');
											if (save_option == 'Y') {
												char filename[101];
												bool file_exists;
												do {
													std::cout << "Enter a filename (max 100 chars): ";
													std::cin.ignore();
													std::cin.getline(filename, 101);
													file_exists = static_cast<bool>(std::ifstream {filename});
													if (file_exists) {
														std::cout << "The file with the given name already exists." << std::endl;
														char option;
														do {
															std::cout << "Do you want to overwrite it (Y/N)? ";
															std::cin >> option;
															option = toupper(option);
															if (option != 'Y' && option != 'N')
																std::cerr << "Invalid option - please select one of Y and N." << std::endl;
														} while (option != 'Y' && option != 'N');
														if (option == 'Y')
															break;
													}
												} while (file_exists);
												std::ofstream output_file {filename};
												output_file << output;
												output_file.close();
											}
											break;
										case 'C':
											output = program.transpile_to_c();
											std::cout << "The program has been transpiled to C." << std::endl;
											{
												char filename[101];
												bool file_exists;
												do {
													std::cout << "Enter a filename (max 100 chars): ";
													std::cin.ignore();
													std::cin.getline(filename, 101);
													file_exists = static_cast<bool>(std::ifstream {filename});
													if (file_exists) {
														std::cout << "The file with the given name already exists." << std::endl;
														char option;
														do {
															std::cout << "Do you want to overwrite it (Y/N)? ";
															std::cin >> option;
															option = toupper(option);
															if (option != 'Y' && option != 'N')
																std::cerr << "Invalid option - please select one of Y and N." << std::endl;
														} while (option != 'Y' && option != 'N');
														if (option == 'Y')
															break;
													}
												} while (file_exists);
												std::ofstream output_file {filename};
												output_file << output;
												output_file.close();
											}
											break;
										case 'J':
											output = program.transpile_to_java();
											std::cout << "The program has been transpiled to Java and will be saved under the name `Program.java`." << std::endl;
											if (std::ifstream {"Program.java"}) {
												std::cout << "The file `Program.java` already exists." << std::endl;
												char option;
												do {
													std::cout << "Would you like to overwrite it (Y/N)? ";
													std::cin >> option;
													option = toupper(option);
													if (option != 'Y' && option != 'N')
														std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (option != 'Y' && option != 'N');
												if (option == 'Y') {
													std::ofstream java_file {"Program.java"};
													java_file << output;
													java_file.close();
												}
											} else {
												std::ofstream java_file {"Program.java"};
												java_file << output;
												java_file.close();
											}
											break;
										case 'P':
											output = program.transpile_to_python();
											std::cout << "The program has been transpiled to Python." << std::endl;
											{
												char filename[101];
												bool file_exists;
												do {
													std::cout << "Enter a filename (max 100 chars): ";
													std::cin.ignore();
													std::cin.getline(filename, 101);
													file_exists = static_cast<bool>(std::ifstream {filename});
													if (file_exists) {
														std::cout << "The file with the given name already exists." << std::endl;
														char option;
														do {
															std::cout << "Do you want to overwrite it (Y/N)? ";
															std::cin >> option;
															option = toupper(option);
															if (option != 'Y' && option != 'N')
																std::cerr << "Invalid option - please select one of Y and N." << std::endl;
														} while (option != 'Y' && option != 'N');
														if (option == 'Y')
															break;
													}
												} while (file_exists);
												std::ofstream output_file {filename};
												output_file << output;
												output_file.close();
											}
											break;
										default:
											std::cerr << "Invalid option - please select one of S, F, C, J, P and Q." << std::endl;
										}
									}
								}
							}
							break;
						case 'I':
							{
								char code[65537];
								std::cout << "Enter your program (max 64KB): ";
								std::cin.ignore();
								std::cin.getline(code, 65537);
								std::cout << std::endl;
								BFToolKit::BFProgram program {*static_cast<BFToolKit::BFProgram *>(interpreter(code).get())};
								char filename[101];
								bool file_exists;
								while (true) {
									std::cout << "S. Read program input from standard input" << std::endl <<
										"F. Read program input from file" << std::endl <<
										"C. Transpile program to C and write result to file" << std::endl <<
										"J. Transpile program to Java and write result to file" << std::endl <<
										"P. Transpile program to Python and write result to file" << std::endl <<
										"Q. Discard current program" << std::endl << std::endl <<
										"Please enter an option: ";
									char option;
									std::cin >> option;
									option = toupper(option);
									std::cout << std::endl;
									char input[1025], save_option;
									std::ifstream input_file;
									std::string output;
									if (option == 'Q')
										break;
									switch (option) {
									case 'S':
										std::cout << "Enter your program input (max 1KB): ";
										std::cin.ignore();
										std::cin.getline(input, 1025);
										output = program(input);
										std::cout << "The program output is: " << output << std::endl;
										do {
											std::cout << "Save program output to file (Y/N)? ";
											std::cin >> save_option;
											save_option = toupper(save_option);
											if (save_option != 'Y' && save_option != 'N')
												std::cerr << "Invalid option - please select one of Y and N." << std::endl;
										} while (save_option != 'Y' && save_option != 'N');
										if (save_option == 'Y') {
											char filename[101];
											bool file_exists;
											do {
												std::cout << "Enter a filename (max 100 chars): ";
												std::cin.ignore();
												std::cin.getline(filename, 101);
												file_exists = static_cast<bool>(std::ifstream {filename});
												if (file_exists) {
													std::cout << "The file with the given name already exists." << std::endl;
													char option;
													do {
														std::cout << "Do you want to overwrite it (Y/N)? ";
														std::cin >> option;
														option = toupper(option);
														if (option != 'Y' && option != 'N')
															std::cerr << "Invalid option - please select one of Y and N." << std::endl;
													} while (option != 'Y' && option != 'N');
													if (option == 'Y')
														break;
												}
											} while (file_exists);
											std::ofstream output_file {filename};
											output_file << output;
											output_file.close();
										}
										break;
									case 'F':
										{
											bool first_time = true;
											do {
												std::cout << "Enter the filename (max 100 chars): ";
												if (first_time) {
													std::cin.ignore();
													first_time = false;
												}
												std::cin.getline(filename, 101);
												file_exists = static_cast<bool>(std::ifstream {filename});
												if (!file_exists)
													std::cerr << "The file you specified does not exist." << std::endl;
											} while (!file_exists);
										}
										input_file.open(filename);
										{
											char temp;
											while (input_file.get(temp))
												output += temp;
										}
										input_file.close();
										output = program(output);
										std::cout << "The program output is: " << output << std::endl;
										do {
											std::cout << "Save program output to file (Y/N)? ";
											std::cin >> save_option;
											save_option = toupper(save_option);
											if (save_option != 'Y' && save_option != 'N')
												std::cerr << "Invalid option - please select one of Y and N." << std::endl;
										} while (save_option != 'Y' && save_option != 'N');
										if (save_option == 'Y') {
											char filename[101];
											bool file_exists;
											do {
												std::cout << "Enter a filename (max 100 chars): ";
												std::cin.ignore();
												std::cin.getline(filename, 101);
												file_exists = static_cast<bool>(std::ifstream {filename});
												if (file_exists) {
													std::cout << "The file with the given name already exists." << std::endl;
													char option;
													do {
														std::cout << "Do you want to overwrite it (Y/N)? ";
														std::cin >> option;
														option = toupper(option);
														if (option != 'Y' && option != 'N')
															std::cerr << "Invalid option - please select one of Y and N." << std::endl;
													} while (option != 'Y' && option != 'N');
													if (option == 'Y')
														break;
												}
											} while (file_exists);
											std::ofstream output_file {filename};
											output_file << output;
											output_file.close();
										}
										break;
									case 'C':
										output = program.transpile_to_c();
										std::cout << "The program has been transpiled to C." << std::endl;
										{
											char filename[101];
											bool file_exists;
											do {
												std::cout << "Enter a filename (max 100 chars): ";
												std::cin.ignore();
												std::cin.getline(filename, 101);
												file_exists = static_cast<bool>(std::ifstream {filename});
												if (file_exists) {
													std::cout << "The file with the given name already exists." << std::endl;
													char option;
													do {
														std::cout << "Do you want to overwrite it (Y/N)? ";
														std::cin >> option;
														option = toupper(option);
														if (option != 'Y' && option != 'N')
															std::cerr << "Invalid option - please select one of Y and N." << std::endl;
													} while (option != 'Y' && option != 'N');
													if (option == 'Y')
														break;
												}
											} while (file_exists);
											std::ofstream output_file {filename};
											output_file << output;
											output_file.close();
										}
										break;
									case 'J':
										output = program.transpile_to_java();
										std::cout << "The program has been transpiled to Java and will be saved under the name `Program.java`." << std::endl;
										if (std::ifstream {"Program.java"}) {
											std::cout << "The file `Program.java` already exists." << std::endl;
											char option;
											do {
												std::cout << "Would you like to overwrite it (Y/N)? ";
												std::cin >> option;
												option = toupper(option);
												if (option != 'Y' && option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (option != 'Y' && option != 'N');
											if (option == 'Y') {
												std::ofstream java_file {"Program.java"};
												java_file << output;
												java_file.close();
											}
										} else {
											std::ofstream java_file {"Program.java"};
											java_file << output;
											java_file.close();
										}
										break;
									case 'P':
										output = program.transpile_to_python();
										std::cout << "The program has been transpiled to Python." << std::endl;
										{
											char filename[101];
											bool file_exists;
											do {
												std::cout << "Enter a filename (max 100 chars): ";
												std::cin.ignore();
												std::cin.getline(filename, 101);
												file_exists = static_cast<bool>(std::ifstream {filename});
												if (file_exists) {
													std::cout << "The file with the given name already exists." << std::endl;
													char option;
													do {
														std::cout << "Do you want to overwrite it (Y/N)? ";
														std::cin >> option;
														option = toupper(option);
														if (option != 'Y' && option != 'N')
															std::cerr << "Invalid option - please select one of Y and N." << std::endl;
													} while (option != 'Y' && option != 'N');
													if (option == 'Y')
														break;
												}
											} while (file_exists);
											std::ofstream output_file {filename};
											output_file << output;
											output_file.close();
										}
										break;
									default:
										std::cerr << "Invalid option - please select one of S, F, C, J, P and Q." << std::endl;
									}
								}
							}
							break;
						default:
							std::cerr << "Invalid option - please select one of F, I and Q." << std::endl;
						}
					}
				}
			}
			break;
		case 'D':
			while (true) {
				std::cout << "F. Read a Brainf**k program from a file" << std::endl <<
					"I. Read a Brainf**k program from standard input" << std::endl <<
					"Q. Discard the current interpreter" << std::endl << std::endl <<
					"Please select an option: ";
				char option;
				std::cin >> option;
				option = toupper(option);
				std::cout << std::endl;
				if (option == 'Q')
					break;
				char code[65537];
				const char *temp;
				int unmatched;
				bool is_valid;
				switch (option) {
				case 'F':
					{
						char filename[101];
						bool file_exists, first_time = true;
						do {
							std::cout << "Please enter the filename (max 100 chars): ";
							if (first_time) {
								std::cin.ignore();
								first_time = false;
							}
							std::cin.getline(filename, 101);
							file_exists = static_cast<bool>(std::ifstream {filename});
							if (!file_exists)
								std::cerr << "The file you specified does not exist." << std::endl;
						} while (!file_exists);
						std::ifstream program_file {filename};
						std::string code;
						int unmatched = 0;
						bool is_valid = true;
						char temp;
						while (program_file.get(temp)) {
							if (temp == '[')
								++unmatched;
							else if (temp == ']')
								--unmatched;
							if (unmatched < 0) {
								is_valid = false;
								break;
							}
							code += temp;
						}
						program_file.close();
						if (!is_valid || unmatched != 0)
							std::cerr << "Invalid program - unmatched square brackets detected!" << std::endl;
						else {
							BFToolKit::BFProgram program {code};
							char filename[101];
							bool file_exists;
							while (true) {
								std::cout << "S. Read program input from standard input" << std::endl <<
									"F. Read program input from file" << std::endl <<
									"C. Transpile program to C and write result to file" << std::endl <<
									"J. Transpile program to Java and write result to file" << std::endl <<
									"P. Transpile program to Python and write result to file" << std::endl <<
									"Q. Discard current program" << std::endl << std::endl <<
									"Please enter an option: ";
								char option;
								std::cin >> option;
								option = toupper(option);
								std::cout << std::endl;
								char input[1025], save_option;
								std::ifstream input_file;
								std::string output;
								if (option == 'Q')
									break;
								switch (option) {
								case 'S':
									std::cout << "Enter your program input (max 1KB): ";
									std::cin.ignore();
									std::cin.getline(input, 1025);
									output = program(input);
									std::cout << "The program output is: " << output << std::endl;
									do {
										std::cout << "Save program output to file (Y/N)? ";
										std::cin >> save_option;
										save_option = toupper(save_option);
										if (save_option != 'Y' && save_option != 'N')
											std::cerr << "Invalid option - please select one of Y and N." << std::endl;
									} while (save_option != 'Y' && save_option != 'N');
									if (save_option == 'Y') {
										char filename[101];
										bool file_exists;
										do {
											std::cout << "Enter a filename (max 100 chars): ";
											std::cin.ignore();
											std::cin.getline(filename, 101);
											file_exists = static_cast<bool>(std::ifstream {filename});
											if (file_exists) {
												std::cout << "The file with the given name already exists." << std::endl;
												char option;
												do {
													std::cout << "Do you want to overwrite it (Y/N)? ";
													std::cin >> option;
													option = toupper(option);
													if (option != 'Y' && option != 'N')
														std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (option != 'Y' && option != 'N');
												if (option == 'Y')
													break;
											}
										} while (file_exists);
										std::ofstream output_file {filename};
										output_file << output;
										output_file.close();
									}
									break;
								case 'F':
									{
										bool first_time = true;
										do {
											std::cout << "Enter the filename (max 100 chars): ";
											if (first_time) {
												std::cin.ignore();
												first_time = false;
											}
											std::cin.getline(filename, 101);
											file_exists = static_cast<bool>(std::ifstream {filename});
											if (!file_exists)
												std::cerr << "The file you specified does not exist." << std::endl;
										} while (!file_exists);
									}
									input_file.open(filename);
									{
										char temp;
										while (input_file.get(temp))
											output += temp;
									}
									input_file.close();
									output = program(output);
									std::cout << "The program output is: " << output << std::endl;
									do {
										std::cout << "Save program output to file (Y/N)? ";
										std::cin >> save_option;
										save_option = toupper(save_option);
										if (save_option != 'Y' && save_option != 'N')
											std::cerr << "Invalid option - please select one of Y and N." << std::endl;
									} while (save_option != 'Y' && save_option != 'N');
									if (save_option == 'Y') {
										char filename[101];
										bool file_exists;
										do {
											std::cout << "Enter a filename (max 100 chars): ";
											std::cin.ignore();
											std::cin.getline(filename, 101);
											file_exists = static_cast<bool>(std::ifstream {filename});
											if (file_exists) {
												std::cout << "The file with the given name already exists." << std::endl;
												char option;
												do {
													std::cout << "Do you want to overwrite it (Y/N)? ";
													std::cin >> option;
													option = toupper(option);
													if (option != 'Y' && option != 'N')
														std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (option != 'Y' && option != 'N');
												if (option == 'Y')
													break;
											}
										} while (file_exists);
										std::ofstream output_file {filename};
										output_file << output;
										output_file.close();
									}
									break;
								case 'C':
									output = program.transpile_to_c();
									std::cout << "The program has been transpiled to C." << std::endl;
									{
										char filename[101];
										bool file_exists;
										do {
											std::cout << "Enter a filename (max 100 chars): ";
											std::cin.ignore();
											std::cin.getline(filename, 101);
											file_exists = static_cast<bool>(std::ifstream {filename});
											if (file_exists) {
												std::cout << "The file with the given name already exists." << std::endl;
												char option;
												do {
													std::cout << "Do you want to overwrite it (Y/N)? ";
													std::cin >> option;
													option = toupper(option);
													if (option != 'Y' && option != 'N')
														std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (option != 'Y' && option != 'N');
												if (option == 'Y')
													break;
											}
										} while (file_exists);
										std::ofstream output_file {filename};
										output_file << output;
										output_file.close();
									}
									break;
								case 'J':
									output = program.transpile_to_java();
									std::cout << "The program has been transpiled to Java and will be saved under the name `Program.java`." << std::endl;
									if (std::ifstream {"Program.java"}) {
										std::cout << "The file `Program.java` already exists." << std::endl;
										char option;
										do {
											std::cout << "Would you like to overwrite it (Y/N)? ";
											std::cin >> option;
											option = toupper(option);
											if (option != 'Y' && option != 'N')
												std::cerr << "Invalid option - please select one of Y and N." << std::endl;
										} while (option != 'Y' && option != 'N');
										if (option == 'Y') {
											std::ofstream java_file {"Program.java"};
											java_file << output;
											java_file.close();
										}
									} else {
										std::ofstream java_file {"Program.java"};
										java_file << output;
										java_file.close();
									}
									break;
								case 'P':
									output = program.transpile_to_python();
									std::cout << "The program has been transpiled to Python." << std::endl;
									{
										char filename[101];
										bool file_exists;
										do {
											std::cout << "Enter a filename (max 100 chars): ";
											std::cin.ignore();
											std::cin.getline(filename, 101);
											file_exists = static_cast<bool>(std::ifstream {filename});
											if (file_exists) {
												std::cout << "The file with the given name already exists." << std::endl;
												char option;
												do {
													std::cout << "Do you want to overwrite it (Y/N)? ";
													std::cin >> option;
													option = toupper(option);
													if (option != 'Y' && option != 'N')
														std::cerr << "Invalid option - please select one of Y and N." << std::endl;
												} while (option != 'Y' && option != 'N');
												if (option == 'Y')
													break;
											}
										} while (file_exists);
										std::ofstream output_file {filename};
										output_file << output;
										output_file.close();
									}
									break;
								default:
									std::cerr << "Invalid option - please select one of S, F, C, J, P and Q." << std::endl;
								}
							}
						}
					}
					break;
				case 'I':
					std::cout << "Please enter your program (max 64KB): ";
					std::cin.ignore();
					std::cin.getline(code, 65537);
					std::cout << std::endl;
					temp = code;
					unmatched = 0;
					is_valid = true;
					while (*temp != '\0') {
						if (*temp == '[')
							++unmatched;
						else if (*temp == ']')
							--unmatched;
						if (unmatched < 0) {
							is_valid = false;
							break;
						}
						++temp;
					}
					if (!is_valid || unmatched != 0)
						std::cerr << "Invalid program - unmatched square brackets detected!" << std::endl;
					else {
						BFToolKit::BFProgram program {code};
						char filename[101];
						bool file_exists;
						while (true) {
							std::cout << "S. Read program input from standard input" << std::endl <<
								"F. Read program input from file" << std::endl <<
								"C. Transpile program to C and write result to file" << std::endl <<
								"J. Transpile program to Java and write result to file" << std::endl <<
								"P. Transpile program to Python and write result to file" << std::endl <<
								"Q. Discard current program" << std::endl << std::endl <<
								"Please enter an option: ";
							char option;
							std::cin >> option;
							option = toupper(option);
							std::cout << std::endl;
							char input[1025], save_option;
							std::ifstream input_file;
							std::string output;
							if (option == 'Q')
								break;
							switch (option) {
							case 'S':
								std::cout << "Enter your program input (max 1KB): ";
								std::cin.ignore();
								std::cin.getline(input, 1025);
								output = program(input);
								std::cout << "The program output is: " << output << std::endl;
								do {
									std::cout << "Save program output to file (Y/N)? ";
									std::cin >> save_option;
									save_option = toupper(save_option);
									if (save_option != 'Y' && save_option != 'N')
										std::cerr << "Invalid option - please select one of Y and N." << std::endl;
								} while (save_option != 'Y' && save_option != 'N');
								if (save_option == 'Y') {
									char filename[101];
									bool file_exists;
									do {
										std::cout << "Enter a filename (max 100 chars): ";
										std::cin.ignore();
										std::cin.getline(filename, 101);
										file_exists = static_cast<bool>(std::ifstream {filename});
										if (file_exists) {
											std::cout << "The file with the given name already exists." << std::endl;
											char option;
											do {
												std::cout << "Do you want to overwrite it (Y/N)? ";
												std::cin >> option;
												option = toupper(option);
												if (option != 'Y' && option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (option != 'Y' && option != 'N');
											if (option == 'Y')
												break;
										}
									} while (file_exists);
									std::ofstream output_file {filename};
									output_file << output;
									output_file.close();
								}
								break;
							case 'F':
								{
									bool first_time = true;
									do {
										std::cout << "Enter the filename (max 100 chars): ";
										if (first_time) {
											std::cin.ignore();
											first_time = false;
										}
										std::cin.getline(filename, 101);
										file_exists = static_cast<bool>(std::ifstream {filename});
										if (!file_exists)
											std::cerr << "The file you specified does not exist." << std::endl;
									} while (!file_exists);
								}
								input_file.open(filename);
								{
									char temp;
									while (input_file.get(temp))
										output += temp;
								}
								input_file.close();
								output = program(output);
								std::cout << "The program output is: " << output << std::endl;
								do {
									std::cout << "Save program output to file (Y/N)? ";
									std::cin >> save_option;
									save_option = toupper(save_option);
									if (save_option != 'Y' && save_option != 'N')
										std::cerr << "Invalid option - please select one of Y and N." << std::endl;
								} while (save_option != 'Y' && save_option != 'N');
								if (save_option == 'Y') {
									char filename[101];
									bool file_exists;
									do {
										std::cout << "Enter a filename (max 100 chars): ";
										std::cin.ignore();
										std::cin.getline(filename, 101);
										file_exists = static_cast<bool>(std::ifstream {filename});
										if (file_exists) {
											std::cout << "The file with the given name already exists." << std::endl;
											char option;
											do {
												std::cout << "Do you want to overwrite it (Y/N)? ";
												std::cin >> option;
												option = toupper(option);
												if (option != 'Y' && option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (option != 'Y' && option != 'N');
											if (option == 'Y')
												break;
										}
									} while (file_exists);
									std::ofstream output_file {filename};
									output_file << output;
									output_file.close();
								}
								break;
							case 'C':
								output = program.transpile_to_c();
								std::cout << "The program has been transpiled to C." << std::endl;
								{
									char filename[101];
									bool file_exists;
									do {
										std::cout << "Enter a filename (max 100 chars): ";
										std::cin.ignore();
										std::cin.getline(filename, 101);
										file_exists = static_cast<bool>(std::ifstream {filename});
										if (file_exists) {
											std::cout << "The file with the given name already exists." << std::endl;
											char option;
											do {
												std::cout << "Do you want to overwrite it (Y/N)? ";
												std::cin >> option;
												option = toupper(option);
												if (option != 'Y' && option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (option != 'Y' && option != 'N');
											if (option == 'Y')
												break;
										}
									} while (file_exists);
									std::ofstream output_file {filename};
									output_file << output;
									output_file.close();
								}
								break;
							case 'J':
								output = program.transpile_to_java();
								std::cout << "The program has been transpiled to Java and will be saved under the name `Program.java`." << std::endl;
								if (std::ifstream {"Program.java"}) {
									std::cout << "The file `Program.java` already exists." << std::endl;
									char option;
									do {
										std::cout << "Would you like to overwrite it (Y/N)? ";
										std::cin >> option;
										option = toupper(option);
										if (option != 'Y' && option != 'N')
											std::cerr << "Invalid option - please select one of Y and N." << std::endl;
									} while (option != 'Y' && option != 'N');
									if (option == 'Y') {
										std::ofstream java_file {"Program.java"};
										java_file << output;
										java_file.close();
									}
								} else {
									std::ofstream java_file {"Program.java"};
									java_file << output;
									java_file.close();
								}
								break;
							case 'P':
								output = program.transpile_to_python();
								std::cout << "The program has been transpiled to Python." << std::endl;
								{
									char filename[101];
									bool file_exists;
									do {
										std::cout << "Enter a filename (max 100 chars): ";
										std::cin.ignore();
										std::cin.getline(filename, 101);
										file_exists = static_cast<bool>(std::ifstream {filename});
										if (file_exists) {
											std::cout << "The file with the given name already exists." << std::endl;
											char option;
											do {
												std::cout << "Do you want to overwrite it (Y/N)? ";
												std::cin >> option;
												option = toupper(option);
												if (option != 'Y' && option != 'N')
													std::cerr << "Invalid option - please select one of Y and N." << std::endl;
											} while (option != 'Y' && option != 'N');
											if (option == 'Y')
												break;
										}
									} while (file_exists);
									std::ofstream output_file {filename};
									output_file << output;
									output_file.close();
								}
								break;
							default:
								std::cerr << "Invalid option - please select one of S, F, C, J, P and Q." << std::endl;
							}
						}
					}
					break;
				default:
					std::cerr << "Invalid option - please select one of F, I and Q." << std::endl;
				}
			}
			break;
		default:
			std::cerr << "Invalid option - please select one of N, D and Q." << std::endl;
		}
	}
	std::cout << "Bye!" << std::endl;
	return 0;
}
