#include "TrivialBFSubstitution.h"
#include "BFProgram.h"
#include <set>

namespace BFToolKit {

// Class constructor of TrivialBFSubstitution
TrivialBFSubstitution::TrivialBFSubstitution(
	const std::string &right_angled_bracket,
	const std::string &left_angled_bracket,
	const std::string &plus_sign,
	const std::string &minus_sign,
	const std::string &period,
	const std::string &comma,
	const std::string &left_square_bracket,
	const std::string &right_square_bracket
):
	Interpreter(),
	RIGHT_ANGLED_BRACKET {right_angled_bracket},
	LEFT_ANGLED_BRACKET {left_angled_bracket},
	PLUS_SIGN {plus_sign},
	MINUS_SIGN {minus_sign},
	PERIOD {period},
	COMMA {comma},
	LEFT_SQUARE_BRACKET {left_square_bracket},
	RIGHT_SQUARE_BRACKET {right_square_bracket} {}

// Conversion to standard Brainf**k "interpreter"
std::unique_ptr<Program> TrivialBFSubstitution::interpret(const std::string &code) const {
	std::string result;
	std::set<std::string> exhausted;
	bool is_exhausted = false;
	size_t idx = 0;
	while (!is_exhausted) {
		std::string cmd;
		size_t pos = std::string::npos, temp;
		if (exhausted.find(RIGHT_ANGLED_BRACKET) == exhausted.end()) {
			temp = code.find(RIGHT_ANGLED_BRACKET, idx);
			if (temp == std::string::npos)
				exhausted.insert(RIGHT_ANGLED_BRACKET);
			else if (temp < pos) {
				cmd = RIGHT_ANGLED_BRACKET;
				pos = temp;
			}
		}
		if (exhausted.find(LEFT_ANGLED_BRACKET) == exhausted.end()) {
			temp = code.find(LEFT_ANGLED_BRACKET, idx);
			if (temp == std::string::npos)
				exhausted.insert(LEFT_ANGLED_BRACKET);
			else if (temp < pos) {
				cmd = LEFT_ANGLED_BRACKET;
				pos = temp;
			}
		}
		if (exhausted.find(PLUS_SIGN) == exhausted.end()) {
			temp = code.find(PLUS_SIGN, idx);
			if (temp == std::string::npos)
				exhausted.insert(PLUS_SIGN);
			else if (temp < pos) {
				cmd = PLUS_SIGN;
				pos = temp;
			}
		}
		if (exhausted.find(MINUS_SIGN) == exhausted.end()) {
			temp = code.find(MINUS_SIGN, idx);
			if (temp == std::string::npos)
				exhausted.insert(MINUS_SIGN);
			else if (temp < pos) {
				cmd = MINUS_SIGN;
				pos = temp;
			}
		}
		if (exhausted.find(PERIOD) == exhausted.end()) {
			temp = code.find(PERIOD, idx);
			if (temp == std::string::npos)
				exhausted.insert(PERIOD);
			else if (temp < pos) {
				cmd = PERIOD;
				pos = temp;
			}
		}
		if (exhausted.find(COMMA) == exhausted.end()) {
			temp = code.find(COMMA, idx);
			if (temp == std::string::npos)
				exhausted.insert(COMMA);
			else if (temp < pos) {
				cmd = COMMA;
				pos = temp;
			}
		}
		if (exhausted.find(LEFT_SQUARE_BRACKET) == exhausted.end()) {
			temp = code.find(LEFT_SQUARE_BRACKET, idx);
			if (temp == std::string::npos)
				exhausted.insert(LEFT_SQUARE_BRACKET);
			else if (temp < pos) {
				cmd = LEFT_SQUARE_BRACKET;
				pos = temp;
			}
		}
		if (exhausted.find(RIGHT_SQUARE_BRACKET) == exhausted.end()) {
			temp = code.find(RIGHT_SQUARE_BRACKET, idx);
			if (temp == std::string::npos)
				exhausted.insert(RIGHT_SQUARE_BRACKET);
			else if (temp < pos) {
				cmd = RIGHT_SQUARE_BRACKET;
				pos = temp;
			}
		}
		if (pos == std::string::npos)
			is_exhausted = true;
		else {
			result += cmd == RIGHT_ANGLED_BRACKET ?
					'>' :
				cmd == LEFT_ANGLED_BRACKET ?
					'<' :
				cmd == PLUS_SIGN ?
					'+' :
				cmd == MINUS_SIGN ?
					'-' :
				cmd == PERIOD ?
					'.' :
				cmd == COMMA ?
					',' :
				cmd == LEFT_SQUARE_BRACKET ?
					'[' :
					']';
			idx = pos + cmd.size();
		}
	}
	return std::unique_ptr<Program> {new BFProgram {result}};
}

}
