# BFToolKit

A simple, minimal C++ library for everything Brainf\*\*k-related, be it a Brainf\*\*k interpreter, transpiler or support for arbitrary TrivialBrainf\*\*kSubstitutions.  MIT Licensed.  See `DOCUMENTATION.md` for details.