all: main
main: main.o Program.o Interpreter.o BFProgram.o TrivialBFSubstitution.o
	g++ -std=c++11 -pedantic-errors -o main main.o Program.o Interpreter.o BFProgram.o TrivialBFSubstitution.o
main.o: main.cpp Interpreter.h Program.h Transpiler.h TrivialBFSubstitution.h BFProgram.h
	g++ -std=c++11 -pedantic-errors -c main.cpp
Program.o: Program.cpp Program.h
	g++ -std=c++11 -pedantic-errors -c Program.cpp
Interpreter.o: Interpreter.cpp Interpreter.h Program.h
	g++ -std=c++11 -pedantic-errors -c Interpreter.cpp
BFProgram.o: BFProgram.cpp BFProgram.h Program.h Transpiler.h
	g++ -std=c++11 -pedantic-errors -c BFProgram.cpp
TrivialBFSubstitution.o: TrivialBFSubstitution.cpp TrivialBFSubstitution.h Interpreter.h Program.h
	g++ -std=c++11 -pedantic-errors -c TrivialBFSubstitution.cpp