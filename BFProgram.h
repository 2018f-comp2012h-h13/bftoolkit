#ifndef BFPROGRAM_H_
#define BFPROGRAM_H_

#include "Program.h"
#include "Transpiler.h"

namespace BFToolKit {
class BFProgram : public Program, public Transpiler {
	/*
	 * A representation of a Brainf**k program which also acts as a transpiler,
	 * i.e. it can transpile itself into its equivalent C, Java and Python program
	 * code
	 */
public:

	// Explicit class constructor (not all strings represent valid Brainf**k
	// programs so allowing implicit conversion does not make sense)
	// Accepts a string representation of a Brainf**k program to construct
	// the corresponding program object
	// N.B. No error checking is performed to ensure that the program passed in is valid
	// It is the responsibility of the caller to ensure that inputs are valid
	// Violation of this assumption causes undefined behavior
	explicit BFProgram(const std::string &code);

	// Execution of a Brainf**k program
	// The program adheres to the following specifications:
	// - The memory tape used is a non-wrapping array of exactly
	//   30,000 unsigned wrapping 8-bit cells initialized to 0 at
	//   the beginning of every execution, with the tape pointer
	//   initially pointing at the leftmost cell
	// - The end-of-file (EOF) character for the program input is
	//   denoted by the NUL character (\0)
	// - Common extensions such as # for debugging and ! for
	//   program/output separation are disabled
	virtual std::string execute(const std::string &input = "") const override;

	// Transpilation of the given program to C99-compliant program code
	// e.g. if the Brainf**k program is ,[.,] (the standard CAT program)
	// and the C99 output is written to a file main.c then:
	// $ gcc -std=c99 -pedantic-errors -o main main.c
	// $ ./main "Hello World"
	// should produce the output "Hello World" followed by a newline
	virtual std::string transpile_to_c() const override;

	// Transpilation of the given program to Java 8 SE compliant program code
	// e.g. if the Brainf**k program is ,[.,] (the standard CAT program)
	// and the Java 8 output is written to a file Program.java then:
	// $ javac Program.java
	// $ java Program "Hello World"
	// should produce the output "Hello World" followed by a newline
	virtual std::string transpile_to_java() const override;

	// Transpilation of the given program to Python 3 compliant program code
	// e.g. if the Brainf**k program is ,[.,] (the standard CAT program)
	// and the Python 3 output is written to a file main.py then:
	// $ python3 main.py "Hello World"
	// should produce the output "Hello World" followed by a newline
	virtual std::string transpile_to_python() const override;

private:
	std::string code; // The string representation of the Brainf**k program
};
}

#endif
