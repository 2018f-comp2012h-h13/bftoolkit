## BFToolKit

A simple yet handy toolkit for interpreting, transpiling and experimenting with Brainf\*\*k programs as well as programs written in arbitrary TrivialBrainf\*\*kSubstitutions.  Includes a few abstract classes for deriving similar tools involving other (esoteric or otherwise) programming languages with a common interface.

### Usage

*NOTE: The instructions below assume you are using a Mac/Linux.  You may need to make minor modifications to the instructions below if you are using Windows.*

If you just want to play around with the toolkit and see what it is capable of, simply download all the files from this repository, fire up the command line, change directories to the folder containing this toolkit, execute `make all` to compile the project files and run the executable `main`.

If you want to make use of the full toolkit in your project, feel free to delete `main.cpp`, `Makefile` and `testcases/`, replace them with your own C++ source files and then compile them as shown below once you are ready (assuming you are using `g++`):

```
$ g++ -std=c++11 -o main <YOUR_PROGRAM_FILES> Interpreter.cpp Program.cpp BFProgram.cpp TrivialBFSubstitution.cpp
```

Provided the compilation is successful, your executable should then be named `main`.  Remember to include the appropriate header files (e.g. `Interpreter.h`, `Program.h`, `Transpiler.h`, `BFProgram.h`, `TrivialBFSubstitution.h`) within your own C++ header/source files as required.

Alternatively, if you do not require the Brainf\*\*k interpreter/transpiler and/or the equivalent for arbitrary TrivialBrainf\*\*kSubstitutions but instead just want to use the provided abstract classes `Interpreter`, `Program` and/or `Transpiler` as a blueprint to design your own toolkit (or otherwise), you can safely delete `BFProgram.h`, `BFProgram.cpp`, `TrivialBFSubstitution.h` and `TrivialBFSubstitution.cpp` on top of `main.cpp`, `testcases/` and `Makefile`, replace them with your own C++ header/source files and compile them with (again assuming `g++`):

```
$ g++ -std=c++11 -o main <YOUR_PROGRAM_FILES> Interpreter.cpp Program.cpp
```

Again, you may need to include the header files `Interpreter.h`, `Program.h` and `Transpiler.h` in your own program files as needed.

Of course, the instructions provided above are merely suggestions - you can mix and match (and perhaps improvise) as you like as long as you keep our MIT License somewhere visible within your project :)

### File Synopsis

The toolkit itself consists of the following files:

- `Program.h` - The class declaration for the `Program` abstract class.
- `Program.cpp` - The corresponding C++ source file for the `Program` abstract class.
- `Interpreter.h` - The class declaration for the `Interpreter` abstract class.
- `Interpreter.cpp` - The corresponding C++ source file for the `Interpreter` abstract class.
- `Transpiler.h` - The class declaration for the `Transpiler` abstract class.  Note that there is no corresponding C++ source file since all member functions of `Transpiler` are pure virtual.
- `BFProgram.h` - The class declaration for the `BFProgram` concrete class.
- `BFProgram.cpp` - The class implementation of `BFProgram`.
- `TrivialBFSubstitution.h` - The class declaration for the `TrivialBFSubstitution` concrete class.
- `TrivialBFSubstitution.cpp` - The class implementation of `TrivialBFSubstitution`.

Additionally, the following files and directories are also included for demonstration purposes (which can be safely deleted if you do not need them):

- `main.cpp` - A demo CLI showcasing some of the potential usages of the `BFToolKit` library.
- `testcases/` - A folder containing some test programs for you to test the toolkit against.  Note that these test programs are not considered part of the toolkit and may therefore be under different license and/or copyright terms - please refer to each individual file for details.
  - `*.b` - Source files containing Brainf\*\*k test programs, many of which are obtained from external sources.  Please refer to each file individually for details regarding licensing/copyright.
  - `*.abcdefgh.txt` - Source files containing test programs written in "abcdefgh" (a TrivialBrainf\*\*kSubstitution), translated from their corresponding `.b` program files with noise (no-ops).
  - `*.reversefuck.txt` - Source files containing test programs written in ReverseF\*\*k, translated from their corresponding `.b` program files with noise.
  - `*.ook.txt` - Source files containing test programs written in Ook!, translated from their corresponding `.b` program files with noise.

### Class Synopsis

All classes in this toolkit are defined under the namespace `BFToolKit`.

- `class Program` - An abstract class modeling an arbitrary program in an arbitrary programming language
  - `public`
    - `virtual std::string execute(const std::string &input = "") const = 0` - A program should be able to accept program input and produce the corresponding program output as an `std::string`.
    - `virtual std::string operator()(const std::string &input = "") const` - An alias for `Program::execute(const std::string & = "")` provided it is not overriden by a derived class, allowing for a more intuitive invocation of the given program (since a program is basically a function in some sense)
    - `virtual void interactive() const` - Executes the given program in interactive mode, asking the user for program input in real time and printing the program output to STDOUT.  Note that program input is limited to 1KB; exceeding the stated limit causes *undefined behavior*.
- `class Interpreter` - An abstract class modeling a generic interpreter for an arbitrary programming language.
  - `public`
    - `virtual std::unique_ptr<Program> interpret(const std::string &code) const = 0` - An interpreter should be able to interpret a program in the given programming language as a string and produce a unique pointer to a `Program` object (`std::unique_ptr<Program>`).
    - `virtual std::unique_ptr<Program> operator()(const std::string &code) const` - An alias of `Interpreter::interpret(const std::string &)` unless overriden by a derived class.  This makes every `Interpreter` instance a functor - it can be invoked like an ordinary function.
- `class Transpiler` - An abstract class modeling an arbitrary transpiler.  Note that derived classes of `Transpiler` need not provide functionality for directly writing the transpilation output to files - it is up to the user and/or the derived class(es) to do so.
  - `public`
    - `virtual std::string transpile_to_c() const = 0` - A transpiler should support transpilation to C program code
    - `virtual std::string transpile_to_java() const = 0` - A transpiler should support transpilation to Java program code
    - `virtual std::string transpile_to_python() const = 0` - A transpiler should support transpilation to Python program code
- `class TrivialBFSubstitution : public Interpreter` - A concrete class for creating interpreters for arbitrary TrivialBrainf\*\*kSubstitutions at runtime
  - `public`
    - `TrivialBFSubstitution(const std::string &right_angled_bracket, const std::string &left_angled_bracket, const std::string &plus_sign, const std::string &minus_sign, const std::string &period, const std::string &comma, const std::string &left_square_bracket, const std::string &right_square_bracket)` - Constructor for an arbitrary TrivialBrainf\*\*kSubstitution interpreter.  The 8 string parameters are substitution tokens for the `>`, `<`, `+`, `-`, `.`, `,`, `[`, `]` commands in ordinary Brainf\*\*k respectively.
    - `virtual std::unique_ptr<Program> interpreter(const std::string &code) const override` - Accepts a program in the given dialect and transforms it into its corresponding `BFProgram`.  Returns a unique pointer to the resulting program.
  - `private`
    - `const std::string RIGHT_ANGLED_BRACKET` - Substitution token for `>` in ordinary Brainf\*\*k
    - `const std::string LEFT_ANGLED_BRACKET` - Substitution token for `<` in ordinary Brainf\*\*k
    - `const std::string PLUS_SIGN` - Substitution token for `+` in ordinary Brainf\*\*k
    - `const std::string MINUS_SIGN` - Substitution token for `-` in ordinary Brainf\*\*k
    - `const std::string PERIOD` - Substitution token for `.` in ordinary Brainf\*\*k
    - `const std::string COMMA` - Substitution token for `,` in ordinary Brainf\*\*k
    - `const std::string LEFT_SQUARE_BRACKET` - Substitution token for `[` in ordinary Brainf\*\*k
    - `const std::string RIGHT_SQUARE_BRACKET` - Substitution token for `]` in ordinary Brainf\*\*k
- `class BFProgram : public Program, public Transpiler` - A concrete class whose instances represent Brainf\*\*k programs which can also be transpiled to C, Java and Python.
  - `public`
    - `explicit BFProgram(const std::string &code)` - Explicit conversion constructor for `BFProgram`: it converts a string representing a Brainf\*\*k program into its corresponding `BFProgram` object.  Implicit conversion is disallowed since an arbitrary string may not represent a valid Brainf\*\*k program; thus allowing implicit conversion does not make sense.
    - `virtual std::string execute(const std::string &input = "") const override` - Member function for non-interactive execution of the given Brainf\*\*k program with the provided program input (or empty if not provided).
    - `virtual std::string transpile_to_c() const override` - Converts the given Brainf\*\*k program into C99-compliant program code.  The resulting C99 program accepts exactly 1 command-line argument as program input (ignoring the program name)
    - `virtual std::string transpile_to_java() const override` - Converts the given Brainf\*\*k program into Java 8 compliant program code.  The resulting Java 8 program accepts exactly 1 command-line argument as program input.
    - `virtual std::string transpile_to_python() const override` - Converts the given Brainf\*\*k program into Python 3 compliant program code.  The resulting Python program accepts exactly 1 command-line argument as program input (ignoring the program name).
  - `private`
    - `std::string code` - The string representation of the Brainf\*\*k program.