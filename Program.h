#ifndef PROGRAM_H_
#define PROGRAM_H_

#include <string>

namespace BFToolKit {
class Program {
	/*
	 * Abstract class for modeling an arbitrary program in an
	 * arbitrary programming language.  An arbitrary program should
	 * at least exhibit the following properties:
	 *
	 * - It can be executed with or without inputs, interactively or otherwise
	 * - It should be a functor, i.e. can be invoked like an ordinary function
	 */
public:

	// A program should be executable with any input and produce
	// the corresponding program output
	// Note that Program::execute(const std::string &) should be non-interactive -
	// the interactive functionality should be provided via Program::interactive()
	// instead
	virtual std::string execute(const std::string &input = "") const = 0;

	// Alias for Program::execute(const std::string &) for more intuitive invocation
	// of a given Program
	virtual std::string operator()(const std::string &input = "") const;

	// A Program should be executable interactively, i.e. asking the user for program
	// input at runtime
	virtual void interactive() const;

	virtual ~Program() = default;
};
}

#endif
